import { group, sleep } from 'k6';
import http from 'k6/http';

// Version: 1.2
// Creator: WebInspector

export let options = {
    duration: '1m',
    vus: 50,
    thresholds: {
      'http_req_duration{expected_response:true}': ['p(95)<1000'],
      'http_req_failed': ['rate<0.1']
    },
};

export default function() {

        var ip = "172.17.0.3:9090";
        //ip = "172.16.250.135:9090";
        var id = 1;


        group("page_7 - http://localhost:9090/index.php?action=ajouter", function() {
                let req, res;
                req = [{
                        "method": "get",
                        "url": "http://"+ip+"/index.php?action=ajouter",
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                }];
                res = http.batch(req);
        });
        group("page_8 -  http://localhost:9090/index.php?action=ajouter", function() {
                let req, res;
                req = [{
                        "method": "post",
                        "url": "http://"+ip+"/index.php?action=ajouter",
                        "body": {
                                "nom": "lepage",
                                "prenom": "test"
                        },
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Cache-Control": "max-age=0",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "Origin": "http://"+ip,
                                        "Content-Type": "application/x-www-form-urlencoded",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php?action=ajouter",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://"+ip+"/index.php",
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Cache-Control": "max-age=0",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Referer": "http://"+ip+"/index.php?action=ajouter",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                }];
                res = http.batch(req);
        });
        group("page_9 -  http://localhost:9090/index.php", function() {
                let req, res;
                req = [{
                        "method": "post",
                        "url": "http://"+ip+"/index.php",
                        "body": {
                                "search": "test"
                        },
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Cache-Control": "max-age=0",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "Origin": "http://"+ip,
                                        "Content-Type": "application/x-www-form-urlencoded",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                }];
                res = http.batch(req);
        });
        group("page_10 -  http://localhost:9090/index.php?action=editer&id=1", function() {
                let req, res;
                req = [{
                        "method": "get",
                        "url": "http://"+ip+"/index.php?action=editer&id="+id,
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                }];
                res = http.batch(req);
        });
        group("page_11 -  http://localhost:9090/index.php?action=editer&id=1", function() {
                let req, res;
                req = [{
                        "method": "post",
                        "url": "http://"+ip+"/index.php?action=editer&id="+id,
                        "body": {
                                "nom": "lepage",
                                "prenom": "tom"
                        },
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Cache-Control": "max-age=0",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "Origin": "http://"+ip,
                                        "Content-Type": "application/x-www-form-urlencoded",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php?action=editer&id="+id,
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://"+ip+"/index.php",
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Cache-Control": "max-age=0",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Referer": "http://"+ip+"/index.php?action=editer&id="+id,
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                }];
                res = http.batch(req);
        });
        group("page_12 -  http://localhost:9090/index.php?action=supprimer&id=1", function() {
                let req, res;
                req = [{
                        "method": "get",
                        "url": "http://"+ip+"/index.php?action=supprimer&id="+id,
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "Referer": "http://"+ip+"/index.php",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://"+ip+"/index.php",
                        "params": {
                                "headers": {
                                        "Host": ip,
                                        "Connection": "keep-alive",
                                        "Upgrade-Insecure-Requests": "1",
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                                        "Sec-Fetch-Site": "same-origin",
                                        "Sec-Fetch-Mode": "navigate",
                                        "Sec-Fetch-User": "?1",
                                        "Sec-Fetch-Dest": "document",
                                        "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"99\", \"Google Chrome\";v=\"99\"",
                                        "sec-ch-ua-mobile": "?0",
                                        "sec-ch-ua-platform": "\"Windows\"",
                                        "Referer": "http://"+ip+"/index.php",
                                        "Accept-Encoding": "gzip, deflate, br",
                                        "Accept-Language": "fr"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://code.jquery.com/jquery-2.0.3.min.js",
                        "params": {
                                "headers": {
                                        "Referer": "http://"+ip,
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "*/*"
                                }
                        }
                },{
                        "method": "get",
                        "url": "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
                        "params": {
                                "headers": {
                                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36",
                                        "Accept": "text/css,*/*;q=0.1"
                                }
                        }
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                },{
                        "method": "get",
                        "url": "https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"
                }];
                res = http.batch(req);

                id++;
                sleep(3);
        });

}