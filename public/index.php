<!DOCTYPE html>
<html>
<head>
    <script data-require="jquery@*" data-semver="2.0.3" src="https://code.jquery.com/jquery-2.0.3.min.js"></script>
    <script data-require="bootstrap@*" data-semver="3.1.1" src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <a href="index.php"><h1> Contact Manager </h1> </a>
</head>


<?php
//include ("invalidInputException.php");
require ("../src/contacts.php");
use function PHPUnit\Framework\throwException;

$id = ""; $nom = ""; $prenom = ""; $recherche = ""; $action = "";

if (isset($_GET['action']))
{
    $action = $_GET['action'];
}

if ($action == 'editer' || $action == 'supprimer')
{

    if (isset($_GET['id']))
    {
        $id = $_GET['id'];
    }

}

//print "Action : ".$action;

$bdd = "";

$nomPC = gethostname();
echo "nom pc: ".$nomPC;
$isGitlab = mb_strrpos($nomPC, 'runner');

if ($isGitlab != false)
{  
    $isGitlab = true;
    $bdd = 'contactsTest.sqlite';
    echo "isGitlab true contactlist : ".$bdd;
}

$contactList;

$contact = new contacts();
 if ($contact->init($bdd))
 {
     // nettoyage liste
    //$contact->deleteAllContact();

    if ($action == null)
    {
        if (isset($_POST['search']))
        {
            $recherche = trim($_POST['search']);
        }
        
        //print "Recherche : ".$recherche;

        if ($recherche != "")
        {
            $contactList = $contact->searchContact($recherche);
            if (count($contactList) < 0)
            {
                print("pas de résultat");
            }
        }
        else
        {
            $contactList = $contact->getAllContacts();
        }
        
    }
    else if ($action == "ajouter")
    {
        
        if (isset($_POST['nom']) && isset($_POST['prenom']))
        {
            $nom = trim($_POST['nom']);
            $prenom = trim($_POST['prenom']);
        }
        

        // si données envoyés
        if ($nom != ""  && $prenom != "")
        {
            print '<br/> prenom : '.$prenom;
            print '<br/> nom : '.$nom. '<br/>'; 

            $success = $contact->createContact($nom, $prenom);

            if ($success)
            {
                echo "ajout contact ok";
                // redirection vers la page de base
                header("Location: index.php");
                die();
            }
        }
    }
    else if ($action == 'supprimer' && $id != '')
    {
        $success = $contact->deleteContact($id);
        if ($success)
        {
            echo "suppression contact ok";
            // redirection vers la page de base
            header("Location: index.php");
            die();
        }
    }

    else if ($action == "editer" && $id != "")
    {
        if (isset($_POST['nom']) && isset($_POST['prenom']))
        {
            $nom = trim($_POST['nom']);
            $prenom = trim($_POST['prenom']);
        }

        // si données envoyés
        if ($nom != ""  && $prenom != "")
        {

            $success = $contact->updateContact($id, $nom, $prenom);

            if ($success)
            {
                echo "edition contact ok";
                // redirection vers la page de base
                header("Location: index.php");
                die();
            }
        }
        // recup des infos du contact
        else
        {
            $success = $contact->getContact($id);

            if ($success)
            {
                $nom = $success['nom'];
                $prenom = $success['prenom'];
            }
        }
    }
    
 }
 else
 {
    print("Erreur connexion bdd");
 }

?>


<?php if ($action == NULL): // on affiche la liste des contacts si pas d'action ?>
<h2>Liste des contacts</h2>

<form method="POST">
    <div class="form-group"> 
        <label for="search">Nom : </label>
        <input type="text" name="search" id="search"
            placeholder="Rechercher un contact"
            class="form-control"
            value = ""
        >
    </div>
    <button type="submit" class="btn btn-primary">Rechercher</button>

</form>


<a href="?action=ajouter"><button class="btn btn-primary mb-1">Ajouter</button></a>
<table class="table table-sm table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($contactList as $c): ?>
    <tr> 
        <td> <?php echo filter_var($c['nom'], FILTER_SANITIZE_SPECIAL_CHARS);?></td>
        <td> <?php echo filter_var($c['prenom'], FILTER_SANITIZE_SPECIAL_CHARS);?></td>
        <td>
            <a class="linkEdit" id=<?php echo $c['id']; ?> href="?action=editer&id=<?php echo $c['id']; ?>">
            <button type="button" id="edit" class="btn btn-link">Modifier</button></a>
            <a href="#" data-toggle="modal" data-target="#confirm-delete" data-href="?action=supprimer&id=<?php echo $c['id']; ?>">
            <button type="button" id="delete" class="btn btn-link" data-toggle="modal" data-href="?action=supprimer&id=<?php echo $c['id']; ?>" data-target="#confirm-delete">Supprimer</button></a>
        </td>  
    </tr>
<?php endforeach; ?>
<?php if (count($contactList) == 0): // si aucun contact ?>
    <tr> 
        <td></td>
        <td> <b>La liste de contacts est vide ! <b> </td>
    </tr>
<?php endif; ?>
    </tbody>
</table>

<?php endif; ?>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
    <div class="modal-dialog"> 
        <div class="modal-content">
             <div class="modal-header"> Suppression du contact </div> 
             <div class="modal-body"> Veuillez confirmer la suppression du contact </div>
              <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button> 
            <a href="" id="deleteConfirm" class="btn btn-danger btn-ok">Supprimer</a>
              </div> </div> </div> </div>

<script>

$('#confirm-delete').on('show.bs.modal',
 function(e)
    { 
     $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href')); 
    }
    );

</script>


<?php if ($action == "ajouter"): ?>
<h2>Ajouter un contact</h2>


<div class=row>
    <div class="col-md-8">
<form method="POST">
    <div class="form-group"> 
        <label for="nom">Nom : </label>
        <input type="text" name="nom" id="nom"
            placeholder="nom"
            class="form-control"
            value = ""
        >
    </div>
    <div class="form-group">
        <label for="prenom">Prénom : </label>
        <input type="text" name="prenom" id="prenom"
            placeholder="prénom"
            class="form-control"
            value = ""
        >
    </div>
    <button type="submit" class="btn btn-primary">Envoyer</button>

</form>
    </div>
</div>



<?php endif; ?>

<?php if ($action == "editer"): ?>
<h2>Edition d'un contact</h2>


<div class=row>
    <div class="col-md-8">
<form method="POST">
    <div class="form-group"> 
        <label for="nom">Nom : </label>
        <input type="text" name="nom" id="nom"
            placeholder="nom"
            class="form-control"
            value = "<?php echo filter_var($nom, FILTER_SANITIZE_SPECIAL_CHARS); ?>"
        >
    </div>
    <div class="form-group">
        <label for="prenom">Prénom : </label>
        <input type="text" name="prenom" id="prenom"
            placeholder="prénom"
            class="form-control"
            value = "<?php echo filter_var($prenom, FILTER_SANITIZE_SPECIAL_CHARS); ?>"
        >
    </div>
    <button type="submit" class="btn btn-primary">Modifier</button>

</form>
    </div>
</div>



<?php endif; ?>
</html>